import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author carli
 */
public class GUIWindow  extends JFrame{
    
    // First line of the GUI, it directs to choosing a file
    JLabel fileChooser = new JLabel("Choose your file! ");
    JTextField fileName = new JTextField("Your file goes here!");
    JButton fileButt = new JButton("File Finder");
    

    // Button to run the program and get a result back    
    JButton result = new JButton("Result");
    
    // A scrollale window where the results text will be shown in.
    JTextArea resultText = new JTextArea("No results yet!");
    JScrollPane resultTextScroll = new JScrollPane(resultText, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
    
    JLabel totalWords = new JLabel("Total number of words:");
    JLabel totalArea = new JLabel("0");
    
    
    JLabel totalDiffWords = new JLabel("Total number of different words:");
    JLabel diffArea = new JLabel("0");
    
    File chosenFile = null;
    
    
    
    public GUIWindow(){
        
        this.setTitle("Greatest word counter ever! ");
        this.setBounds(300, 200, 800, 600);      // Size of the GUI window that will open
        this.getContentPane().setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        // Creates the text file with its size and its location
        this.fileButt.setBounds(600, 50, 140, 30);
        this.getContentPane().add(fileButt);
        this.fileButt.addActionListener(new FileButtonListener());
        
        // Creates the text box where the file name will be shown with its size and its location
        this.fileName.setBounds(140, 50, 460, 30);
        this.getContentPane().add(fileName);
        
        // Creates a text that tells the user to choose a file with its size and its location
        this.fileChooser.setBounds(40, 50, 100, 30);
        this.getContentPane().add(fileChooser);
        
        // Creates the button to run the program with its size and its location
        this.result.setBounds(345, 110, 100, 30);
        this.getContentPane().add(result);
        this.result.addActionListener(new ButtonListener());
        
        // Creates the text area with scrollable stuff with its size and its location
        this.resultTextScroll.setBounds(40, 175, 700, 250);
        this.getContentPane().add(resultTextScroll);
        
        // Creates JLabel for total words
        this.totalWords.setBounds(40, 450, 400, 30);
        this.getContentPane().add(totalWords);
        
        // Creates JLabel for total different words
        this.totalDiffWords.setBounds(40, 500, 400, 30);
        this.getContentPane().add(totalDiffWords);
        
        // An area for the number of total words to be display at
        this.totalArea.setBounds(400, 450, 20, 20);
        this.getContentPane().add(totalArea);
        
        // An area for the number of total different words to be display at
        this.diffArea.setBounds(400, 500, 20, 20);
        this.getContentPane().add(diffArea);
    
    }

    private void theButtonHasBeenPushed() {
        
        Driver wordCount = new Driver(chosenFile);
        
        // Grabs the information from the main where it calculates and spits out a string full of the results for creating a resolve string
        this.resultText.setText(wordCount.getAnswer());
        
        // Grabs the file path and puts it inside a text box
        this.fileName.setText(chosenFile.getPath());
        
        // Grabs the total number of words and puts it inside a JLabel
        this.totalArea.setText(wordCount.getTotal());
        
        // Grabs the total number of different words and puts it inside a JLabel
        this.diffArea.setText(wordCount.getTotalDiff());
        
    }
    
    
    private class ButtonListener implements ActionListener {
        
        @Override
        public void actionPerformed( ActionEvent e) {
            GUIWindow.this.theButtonHasBeenPushed();
        }
    }
    
    
    private class FileButtonListener implements ActionListener {
        @Override
        public void actionPerformed( ActionEvent e) {
            System.out.println("hit the file button");
            JFileChooser chooser = new JFileChooser();
            chooser.setFileFilter( new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
            System.out.println("I created the file chooser");
            int chooserSuccess = chooser.showOpenDialog( null);
            System.out.println("I open the file chooser, it returned " + chooserSuccess);
            if( chooserSuccess == JFileChooser.APPROVE_OPTION) {
                 chosenFile = chooser.getSelectedFile();
                // Pass this file to your function
                System.out.println("You chose the file " + chosenFile.getAbsolutePath());
                System.out.println("You chose the file " + chosenFile.getName());
            }
            else {
                System.out.println("You hit cancel");
            }
        }
    }
    
    
}
