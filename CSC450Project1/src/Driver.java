import java.util.HashMap;
import java.util.Map;
import java.io.File;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeMap;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author carli
 */
public class Driver {
    
    // Creating 3 variables that will carry the 3 values needed over to the GUI
   
    private String answer = null;
    private int totalWord = 0;
    private int totalDiff = 0;
   
    // Created 3 getters for the 3 values needed to show in the GUI

    /**
     * Creates a string in which all the sorted data from the file has been assorted
     * by alphabetical order.
     * @return answer
     */
    public String getAnswer(){
    
        return answer;
    
    }
    
    /**
     * This function returns the value of how many words there are in the file.
     * @return t 
     */
    public String getTotal(){
        String t = String.valueOf(totalWord);   // Changed the value of total words to a String so it can be printed on the JLabel
        return t;
    
    }
    
    /**
     * This returns the value of how many different words are in the given file.
     * @return e
     */
    public String getTotalDiff(){
        String e = String.valueOf(totalDiff);   // Changed the value of total different words to a String so it can be printed on the JLabel
        return e;
    
    }
    
    /**
     * @param args the command line arguments
     */
    public Driver(File fileName) {
        // TODO code application logic here
        
        // Checks if the file can be found and if it is, it will be added to the variable file.
        try{
            
            Scanner file = new Scanner(fileName);
            
            // Creating HashMap called hm with Strings and Integers.
        HashMap<String, Integer> hm = new HashMap<>();
        
        
        // While the file has a word that has not been altered, the program will continue to run until it is done.
        while(file.hasNext()){
            
            /*
            ** Adding the information from the file into a String array but first altering the whole file when needed.
            ** First it replaces all the letters that are not a-z to nothing, then lowercases all words and splits if there is an operation such as \n or \t.
            */
            String[] temp = file.next().replaceAll("[^a-zA-Z]", " ").toLowerCase().split("\\s+");
            
            // Creates HashMap with all words that have been put into the String array and running a for loop as big as the lenght of that array.
            for (int i=0; i<temp.length ; i++) {
              if (hm.containsKey(temp[i])) {
                 int cont = hm.get(temp[i]);
                 hm.put(temp[i], cont + 1);
              } 
              else {
                 hm.put(temp[i], 1);
              }
            }
        }
            
        this.answer = createResolveString(hm);
        this.totalDiff = totalDifferentWordCount(hm);
        this.totalWord = totalWordCount(hm);
            
        }
        catch(Exception e){
            // Error message for not finding the file.
            System.out.println("File cannot be found! ");
            System.exit(-1);
        }
        
    }
    
    /**
     * This function calculates all the words inside the document, even if they are repeated words.
     * @param hm
     * @return count
     */
    public static int totalWordCount(HashMap<String, Integer> hm){
        
        int count = 0;
        
        for(HashMap.Entry<String, Integer> entry : hm.entrySet()){   
            count += entry.getValue();       // This line keeps on counting for every single time a word has been repeated
        }
        return count;
    }
    
    /**
     * This function gives you the total number of different words inside the text file with no repeats.
     * @param hm
     * @return The size of the HashMap hm
     */
    public static int totalDifferentWordCount(HashMap hm){
        
        return hm.size();
    }
    
    /**
     * This function creates a list of all the words in the file plus creates an output for it as a list with the number of times it was repeated.
     * @param hm
     */
    public static String createResolveString(HashMap<String, Integer> hm){
        
        String data = "Generating the list of words with its value numbers below! \n ----------------------------------------------------------------------- \n";
            
            TreeMap<String, Integer> sorted = new TreeMap<>();
            sorted.putAll(hm);

            for(Map.Entry<String, Integer> entry : sorted.entrySet()){
                data += String.format("   " + "%-15s = %-15d", entry.getKey(), entry.getValue()) + "\n";
            }
            
//            for(HashMap.Entry<String, Integer> entry : hm.entrySet()){
//                data += String.format("   " + "%-15s = %-15d", entry.getKey(), entry.getValue()) + "\n";
//                
//            }
            System.out.println(data);
            return data;
    }
    
}
